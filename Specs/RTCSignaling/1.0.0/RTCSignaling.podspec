Pod::Spec.new do |s|
  s.name         = "RTCSignaling"
  s.version      = "1.0.0"
  s.summary      = "iOS compiled WebRTC libraries"
  s.homepage     = "https://github.com/steamclock/webrtc-ios"
  s.license      = { :type => "OTHER", :file => "LICENSE" }
  s.author       = { "&yet" => "contact@andyet.com" }

  s.platform = :osx
  s.platform = :ios

  s.ios.deployment_target = '8.0'
 # s.osx.deployment_target = '10.9'

  s.source       = { :git => "https://zaigham_mt@bitbucket.org/WGProjects/rtcsignaling.git" }
  s.source_files  = 'RTCSignaling', 'RTCSignaling/**/*.{h,m}'
  
  s.vendored_libraries = "lib/*.a"
  s.requires_arc = true
  s.libraries = 'sqlite3', 'stdc++', 'icucore', 'c++.1', 'resolv'
  s.frameworks = 'AVFoundation', 'AudioToolbox', 'CoreMedia', 'CoreVideo', 'GLKit', 'OpenGLES', 'Security', 'MediaPlayer', 'AudioToolbox', 'QuartzCore', 'CoreGraphics', 'VideoToolbox', 'MobileCoreServices'

 s.xcconfig = { 
                "HEADER_SEARCH_PATHS" => "$(PODS_ROOT)/RTCSignaling/RTCSignaling/**/",
                "LIBRARY_SEARCH_PATHS" => "$(PODS_ROOT)/RTCSignaling/RTCSignaling/RTCLibrary/lib/",
                "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES",
                "OTHER_LDFLAGS" => "\"-lWebRTC\"",
                "ENABLE_BITCODE" => "NO"
  }

#  s.dependency 'RTCLibrary'
#"OTHER_LDFLAGS" => "\"-lWebRTC\"",
#"ENABLE_BITCODE" => "NO"

s.subspec 'RTCLibrary' do |rt|
  rt.source_files = 'RTCLibrary/include/**/*.{h}'
  rt.vendored_libraries = "RTCLibrary/lib/*.a"
  rt.ios.exclude_files = 'RTCLibrary/include/RTCNSGLVideoView.{h}'
  rt.xcconfig = {
        "HEADER_SEARCH_PATHS" => "$(PODS_ROOT)/RTCSignaling/RTCLibrary/include/",
        "LIBRARY_SEARCH_PATHS" => "\"$(PODS_ROOT)/RTCSignaling/RTCLibrary/lib/\"",
        #"CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES",
        
      }
end

=begin
  "subspecs": [

    {
      "name": "RTCLibrary",
      "source_files": [
        "RTCLib/include/**/*.{h}"
      ],
      "vendored_libraries": "RTCLib/lib/*.a",

        "xcconfig": {
        "HEADER_SEARCH_PATHS": "$(PODS_ROOT)/RTCSignaling/RTCLibrary/include/",
        "LIBRARY_SEARCH_PATHS": "\"$(PODS_ROOT)/RTCSignaling/RTCLibrary/lib/\"",
        "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES": "YES",
        "OTHER_LDFLAGS": "\"-lWebRTC\"",
        "ENABLE_BITCODE": "NO"
      }
  
    }
  ]
=end
end