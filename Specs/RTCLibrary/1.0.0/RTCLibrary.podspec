Pod::Spec.new do |s|
  s.name         = "RTCLibrary"
  s.version      = "1.0.0"
  s.summary      = "iOS compiled WebRTC libraries"
  s.homepage     = "https://bitbucket.org/WGProjects/rtc-lib.git"
  s.license      = { :type => "OTHER", :file => "LICENSE" }
  s.author       = { "&yet" => "zaigham.maqsood@liveadmins.com" }
  s.platform     = :ios, '7.0'
  s.source       = { :git => "https://zaigham_mt@bitbucket.org/WGProjects/rtc-lib.git"}
  s.source_files = "include/**/*.{h}"
  s.ios.exclude_files = 'RTCLibrary/include/RTCNSGLVideoView.h'
  s.vendored_libraries = "lib/*.a"
  #s.preserve_paths = "lib/*.a"
#  s.requires_arc = false
 # s.libraries = 'sqlite3', 'stdc++'
 # s.framework = 'AVFoundation', 'AudioToolbox', 'CoreMedia'
 # s.xcconfig     = {
 #   'OTHER_LDFLAGS'  => '-stdlib=libstdc++',
#	'ARCHS' => 'armv7',
#	'ONLY_ACTIVE_ARCH' => 'NO'
 # }
end
