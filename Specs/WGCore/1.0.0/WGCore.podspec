Pod::Spec.new do |s|  
  s.name         = "WGCore"
  s.version      = "1.0.0"
  s.summary      = "it contains the XMPP integrated project."

  s.description  = <<-DESC
                   A longer description of WGCore in Markdown format.
                   DESC

  s.homepage     = "https://zaigham_mt@bitbucket.org/WGProjects/wgcore.git"
  s.license      = { :type => 'MIT'}
  s.author       = { "LiveAdmins" => "zaigham.maqsood@liveadmins.com" }

  s.platform = :osx, '10.9'
  s.ios.platform = :ios, '7.0'
  s.ios.deployment_target = '7.0'
  s.osx.platform = :osx, '10.8'
  s.osx.deployment_target = '10.9'
  
  s.source       = { :git => "https://zaigham_mt@bitbucket.org/WGProjects/wgcore.git" ,:branch => 'Dev'}

  s.source_files  = 'WGCore', 'WGCore/**/*.{h,m}'
  s.public_header_files = 'WGCore/**/*.h'
  s.resources    = 'WGCore/**/*.{wav,png,sqlite,xcdatamodeld}'
  s.framework    = 'Foundation'
  s.requires_arc = true
  # s.library = "libxml2"
  s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  s.osx.dependency 'XMPPFramework', '<= 3.6.4'
  s.ios.dependency 'XMPPFramework'
  #s.dependency 'AFNetworking', '<= 2.5.4'
  s.dependency 'FMDB'
  s.dependency 'Reachability'
  #s.dependency 'Socket.IO-Client-Swift'
  #s.dependency 'SocketRocket'
  s.dependency 'SignalR-ObjC', '2.0.0.beta3'
  #s.dependency 'SignalR-ObjC'

end  