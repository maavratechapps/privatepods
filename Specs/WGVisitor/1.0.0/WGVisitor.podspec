Pod::Spec.new do |s|  
  s.name         = "WGVisitor"
  s.version      = "1.0.0"
  s.summary      = "it contains the XMPP intergrated project."
  s.ios.deployment_target = '8.0'
  s.description  = <<-DESC
                   A longer description of WGCore in Markdown format.
                   DESC

  s.homepage     = "http://EXAMPLE/WGCore"
  s.license      = { :type => 'MIT'}
  s.author       = { "LiveAdmins" => "zaigham.maqsood@liveadmins.com" }
  s.platform     = :ios, '7.0'
  s.source       = { :git => "zaigham_mt@bitbucket.org:maavratechapps/wgvisitor.git"}
  s.source_files = 'WGCore/**/*.{h,m}', 'WGVisitor/**/*.{h,m}'
  s.public_header_files = 'WGCore/**/*.h'
  #s.resources    = 'WGCore/*.png', 'WGVisitor/**/*.{png,jpeg,jpg,storyboard,xib}'
  #,'WGCore/*.png
   s.resource_bundles = {
     'WGVisitorRes' => ['WGVisitor/**/*.{png,jpeg,jpg,storyboard,xib}', 'WGCore/**/*.{png,jpeg,jpg,storyboard,xib}' ]
 }
  s.framework    = 'Foundation'
  s.requires_arc = true
  # s.library = "libxml2"
  s.xcconfig = {
'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2 $(SDKROOT)/usr/include/libresolv $(PODS_ROOT)/XMPPFramework/module $(PODS_ROOT)/WGVisitor/WGVisitor ',
'LIBRARY_SEARCH_PATHS' => "$(PODS_ROOT)/RTCSignaling/RTCLibrary/lib/",
#'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES',
"ENABLE_BITCODE" => "NO",
'OTHER_LDFLAGS' => '"$(inherited)" "-lxml2" "-objc" -l"WebRTC"'
}

s.libraries = 'sqlite3', 'stdc++', 'icucore', 'c++.1', 'resolv'
  s.frameworks = 'AVFoundation', 'AudioToolbox', 'CoreMedia', 'CoreVideo', 'GLKit', 'OpenGLES', 'Security', 'MediaPlayer', 'AudioToolbox', 'QuartzCore', 'CoreGraphics', 'VideoToolbox', 'MobileCoreServices'

#pod 'AFNetworking', '~> 2.0'
  s.dependency 'XMPPFramework'
  s.dependency 'Reachability'
  s.dependency 'OAStackView'
  s.dependency 'RTCSignaling'
  s.dependency 'Socket.IO-Client-Swift'
  s.dependency 'TPKeyboardAvoiding'
  


  
end  